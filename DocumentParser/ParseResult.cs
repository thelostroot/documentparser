﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentParser
{
    public class ParseResult
    {
        public Dictionary<string, string> Parsed { get; set; }
        public List<TemplateNode> NotFound { get; set; }
    }
}
