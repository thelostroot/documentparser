﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;


namespace DocumentParser
{
    class WordProvider : DocumentProvider
    {
        public WordProvider()
        {

        }

        public string GetText(string filePath)
        {
            string text = "\n";
            using (WordprocessingDocument wordDocument =
                WordprocessingDocument.Open(filePath, false))
            {               
                Body body = wordDocument.MainDocumentPart.Document.Body;
                var childs = body.ChildElements.ToList();

                foreach (var child in childs)
                    text += child.InnerText + '\n';

                return text;
            }
        }
    }
}
