﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentParser
{
    interface DocumentProvider
    {
        string GetText(string filePath);
    }
}
