﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace DocumentParser
{
    class DBProvider
    {
        private NpgsqlConnection conn;

        public bool Connect(string server, string port, string user, string pass, string database)
        {
            try
            {
                conn = new NpgsqlConnection("Server=" + server +
                                       ";Port=" + port +
                                       ";User Id=" + user +
                                       ";Password=" + pass +
                                       ";Database=" + database +
                                       ";");
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void CreateTable(string name, List<string> fileds)
        {
            string q = "CREATE TABLE " + name + " (";
            q += "id SERIAL, ";

            var t = new List<string>();
            foreach (var filed in fileds)
                t.Add(filed + " TEXT");
            q += string.Join(", ", t);

            q += ")";

            // create command
            NpgsqlCommand cmd = new NpgsqlCommand(q, conn);

            // do command
            conn.Open();
            NpgsqlDataReader dr = cmd.ExecuteReader();

            conn.Close();
        }

        private string BuildInsertParamString(Dictionary<string, string> data)
        {
            string q ="";

            string keys = string.Join(",", data.Keys);
            string vals = string.Join(",@", data.Keys );
            vals = "@" + vals;

            q = "(" + keys + ") VALUES (" + vals + ")";

            return q;
        }

        public int Insert(string table, Dictionary<string, string> data)
        {
            string q = "INSERT INTO " + table + " " + BuildInsertParamString(data) + ";SELECT currval('" + table + "_id_seq'); ";

            // create command
            NpgsqlCommand cmd = new NpgsqlCommand(q, conn);

            // set params
            foreach (var item in data)            
                cmd.Parameters.AddWithValue("@" + item.Key, item.Value);
            

            // do command
            conn.Open();
            NpgsqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            int insertID = Convert.ToInt32(dr[0]);

            conn.Close();
            return insertID;
        }
    }
}
