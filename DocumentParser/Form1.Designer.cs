﻿namespace DocumentParser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadDocumentBTN = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.loadTemplateBTN = new System.Windows.Forms.Button();
            this.parseBTN = new System.Windows.Forms.Button();
            this.exportBdBTN = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // loadDocumentBTN
            // 
            this.loadDocumentBTN.Location = new System.Drawing.Point(540, 12);
            this.loadDocumentBTN.Name = "loadDocumentBTN";
            this.loadDocumentBTN.Size = new System.Drawing.Size(134, 23);
            this.loadDocumentBTN.TabIndex = 0;
            this.loadDocumentBTN.Text = "Загрузить документ";
            this.loadDocumentBTN.UseVisualStyleBackColor = true;
            this.loadDocumentBTN.Click += new System.EventHandler(this.loadDocumentBTN_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(522, 289);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // loadTemplateBTN
            // 
            this.loadTemplateBTN.Location = new System.Drawing.Point(540, 41);
            this.loadTemplateBTN.Name = "loadTemplateBTN";
            this.loadTemplateBTN.Size = new System.Drawing.Size(134, 23);
            this.loadTemplateBTN.TabIndex = 2;
            this.loadTemplateBTN.Text = "Загрузить шаблон\r\n";
            this.loadTemplateBTN.UseVisualStyleBackColor = true;
            this.loadTemplateBTN.Click += new System.EventHandler(this.loadTemplateBTN_Click);
            // 
            // parseBTN
            // 
            this.parseBTN.Location = new System.Drawing.Point(540, 92);
            this.parseBTN.Name = "parseBTN";
            this.parseBTN.Size = new System.Drawing.Size(134, 23);
            this.parseBTN.TabIndex = 3;
            this.parseBTN.Text = "Парсинг";
            this.parseBTN.UseVisualStyleBackColor = true;
            this.parseBTN.Click += new System.EventHandler(this.parseBTN_Click);
            // 
            // exportBdBTN
            // 
            this.exportBdBTN.Location = new System.Drawing.Point(540, 121);
            this.exportBdBTN.Name = "exportBdBTN";
            this.exportBdBTN.Size = new System.Drawing.Size(134, 23);
            this.exportBdBTN.TabIndex = 4;
            this.exportBdBTN.Text = "Экспорт в БД";
            this.exportBdBTN.UseVisualStyleBackColor = true;
            this.exportBdBTN.Click += new System.EventHandler(this.exportBdBTN_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 323);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(235, 82);
            this.listBox1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 307);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Не найденые параметры";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox2.Location = new System.Drawing.Point(253, 323);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(281, 82);
            this.richTextBox2.TabIndex = 7;
            this.richTextBox2.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 307);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Шаблон парсера";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 412);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.exportBdBTN);
            this.Controls.Add(this.parseBTN);
            this.Controls.Add(this.loadTemplateBTN);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.loadDocumentBTN);
            this.Name = "Form1";
            this.Text = "DocumentParser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadDocumentBTN;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button loadTemplateBTN;
        private System.Windows.Forms.Button parseBTN;
        private System.Windows.Forms.Button exportBdBTN;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

