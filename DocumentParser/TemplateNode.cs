﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentParser
{
    public class TemplateNode
    {
        public string StartPharse { get; set; }
        public string ColumnName { get; set; }
        public string EndPharse { get; set; }
    }
}
