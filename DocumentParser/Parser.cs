﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DocumentParser
{
    class Parser
    {
        WordProvider wp;
        public string documentText;
        List<TemplateNode> tpls;

        public Parser()
        {
            wp = new WordProvider();
        }       

        public void LoadDocument(string filePath)
        {
            documentText = wp.GetText(filePath);
        }

        public void LoadTemplate(string 
            filePath)
        {
            var templateLoader = new TemplateLoader();
            tpls = templateLoader.LoadTemplate(filePath);
        }

        public ParseResult Parse()
        {
            var parsed = new Dictionary<string, string>();
            var notFound = new List<TemplateNode>();
            var t = documentText.IndexOf("\n", 1);

            // Search match for TemplateNode
            foreach (var tpl in tpls)
            {
                int i = documentText.IndexOf(tpl.StartPharse);
                if (i == -1)
                {
                    notFound.Add(tpl);
                    continue;
                }

                int j = documentText.IndexOf(tpl.EndPharse, i);
                if (i == -1)
                {
                    notFound.Add(tpl);
                    continue;
                }

                string val = documentText.Substring(i + tpl.StartPharse.Length, j - i - tpl.StartPharse.Length );
                parsed.Add(tpl.ColumnName, val);
            }

            var res = new ParseResult();
            res.Parsed = parsed;
            res.NotFound = notFound;
            return res;
        }

        
    }
}
