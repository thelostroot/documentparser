﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DocumentParser
{
    class TemplateLoader
    {
        public List<TemplateNode> LoadTemplate(string filePath)
        {
            List<TemplateNode> res = new List<TemplateNode>();
            string s = File.ReadAllText(filePath);

            int i = 0;
            while (i < s.Length)
            {
                TemplateNode tpl = new TemplateNode();
                tpl.StartPharse = NextNode(s, ref i);
                tpl.ColumnName = NextNode(s, ref i);
                tpl.EndPharse = NextNode(s, ref i);
                // Next char
                i++;
                res.Add(tpl);
            }

            return res;
        }

        private string NextNode(string s, ref int i)
        {
            // Search open [
            i = s.IndexOf("[", i) + 1;

            // Search close ]
            int j = s.IndexOf("]", i);

            // Get value
            string val = s.Substring(i, j - i);
            if (val == "end-par")
                val = "\n";

            // Go to close ]
            i = j;

            return val;
        }
    }
}
