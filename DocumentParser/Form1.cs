﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DocumentParser
{
    public partial class Form1 : Form
    {
        Parser parser;
        ParseResult pr;
        public Form1()
        {
            InitializeComponent();
            parser = new Parser();
        }

        private void loadDocumentBTN_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                parser.LoadDocument(openFileDialog1.FileName);
                richTextBox1.Text = parser.documentText;                
            }       
        }       

        private void loadTemplateBTN_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                richTextBox2.Text = File.ReadAllText(openFileDialog1.FileName);
                parser.LoadTemplate(openFileDialog1.FileName);
            }
        }

        private void parseBTN_Click(object sender, EventArgs e)
        {
            pr = parser.Parse();

            // Show not found nodes
            listBox1.Items.Clear();
            foreach (var item in pr.NotFound)
            {
                listBox1.Items.Add("[" + item.StartPharse + "][" + item.ColumnName + "][" + item.EndPharse + "]");
            }

            // Show match nodes
            richTextBox1.Text = "Данные для вставки в БД:" + Environment.NewLine;
            foreach (var item in pr.Parsed)
            {
                richTextBox1.Text += item.Key + ":" + Environment.NewLine + item.Value + Environment.NewLine + Environment.NewLine;
            }
        }

        private void exportBdBTN_Click(object sender, EventArgs e)
        {
            DBProvider dbProvider = new DBProvider();
            dbProvider.Connect("localhost", "5432", "postgres", "1", "parser");
            dbProvider.CreateTable("parsed", pr.Parsed.Keys.ToList() );
            dbProvider.Insert("parsed", pr.Parsed);
        }
    }
}
